<?php
require __DIR__ . '/vendor/autoload.php';

use User\UserClient;
use User\UserEntity;
use User\UserIndexRequest;
use User\UserIndexResponse;

go(function () {
    $userClient = new UserClient('10.10.31.211:50051', [
        'credentials' => Grpc\ChannelCredentials::createInsecure()
    ]);
    $userClient->start();
    $request = new UserIndexRequest();
    $request->setPage(1);
    $request->setPageSize(15);

    /* @var $userIndexResponse UserIndexResponse*/
    list($userIndexResponse, $status) = $userClient->UserIndex($request);

    var_dump($status);

    $err = $userIndexResponse->getErr();
    $msg = $userIndexResponse->getMsg();

    printf("user index request: err %d msg %s \n", $err, $msg);
    /* @var UserEntity[] */
    $data = $userIndexResponse->getData();

    foreach ($data as $row) {
        echo $row->getName() . " " . $row->getAge() . PHP_EOL;
    }
    $userClient->close();
});