<?php

require_once __DIR__ . '/vendor/autoload.php';

use Grpc\Parser;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Swoole\Http\Server;
use User\UserIndexRequest;
use User\UserIndexResponse;


$http = new Server('0.0.0.0', 50051, SWOOLE_BASE);
$http->set([
    'log_level'           => SWOOLE_LOG_INFO,
    'trace_flags'         => 0,
    'worker_num'          => 1,
    'open_http2_protocol' => true
]);

$http->on('workerStart', function (Server $server) {
    echo "grpc server start" . PHP_EOL;
});

$http->on('request', function (Request $request, Response $response) use ($http) {
    $path  = $request->server['request_uri'];
    $route = [
        '/user.User/UserIndex' => function (...$args) {
            [$server, $request, $response] = $args;
            $userIndexRequest = Grpc\Parser::deserializeMessage([UserIndexRequest::class, null], $request->rawContent());
            if ($userIndexRequest) {
                printf(
                    "get user index request: page %d page_size %d\n",
                    $userIndexRequest->getPage(),
                    $userIndexRequest->getPageSize()
                );

                $userIndexResponse = new UserIndexResponse();
                $userIndexResponse->setErr(0);
                $userIndexResponse->setMsg("success");
                $userIndexResponse->setData([
                    new \User\UserEntity([
                        'name' => 'sqrt_cat',
                        'age'  => 25
                    ]),
                    new \User\UserEntity([
                        'name' => 'big_cat',
                        'age'  => 25
                    ]),
                ]);
                $response->header('content-type', 'application/grpc');
                $response->header('trailer', 'grpc-status, grpc-message');
                $trailer = [
                    "grpc-status"  => "0",
                    "grpc-message" => ""
                ];
                foreach ($trailer as $trailer_name => $trailer_value) {
                    $response->trailer($trailer_name, $trailer_value);
                }
                $response->end(Grpc\Parser::serializeMessage($userIndexResponse));
                return true;
            }
            return false;
        }
    ];

    if (!(isset($route[$path]) && $route[$path]($http, $request, $response))) {
        $response->status(400);
        $response->end('Bad Request');
    }
});

$http->start();
